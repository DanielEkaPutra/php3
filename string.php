<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
        echo "<h3>Soal No 1</h3>";
        $first_sentence = "Hello PHP!";
        $panjang = strlen($first_sentence);
        $jumlah = str_word_count($first_sentence);
        echo "<p>$first_sentence</p>";
        echo "<p>Panjang String = $panjang</p>";
        echo "<p>Jumlah Kata = $jumlah</p>";
        echo "<br>";
        $second_sentence = "I'm ready for the challenges";
        $panjang = strlen($second_sentence);
        $jumlah = str_word_count($second_sentence);
        echo "<p>$second_sentence</p>";
        echo "<p>Panjang String = $panjang</p>";
        echo "<p>Jumlah Kata = $jumlah</p>";
        echo "<br>";

        echo "<h3>Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "<label>String: $string2</label><br>";
        echo "Kata Pertama: " . substr($string2, 0, 1) . "<br>";
        echo "Kata Kedua: " . substr($string2, 2, 4) . "<br>";
        echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br><br>";
        
        echo "<h3>Soal No 3</h3>";
        $string3 = "PHP is old but sexy!";
        $ubah = str_replace("sexy", "awesome", $string3);
        echo "<label>Before: $string3</label><br>";
        echo "<label>After: $ubah</label><br>";
        
        
    ?>
</body>
</html>