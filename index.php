<!DOCTYPE html>
<?php

    $judul = "ini halaman index";

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $judul?></title>
</head>
<body>
    <h1>Test HTML</h1>
    <?php
        echo "<h3>Contoh No 1</h3>";
        $kalimat1 = "Hello World!";
        echo "<p>$kalimat1</p>";
        $jumlah_karakter1 = strlen($kalimat1);
        $jumlah_kata1 = str_word_count($kalimat1);
        echo "<p>Jumlah Karakter = $jumlah_karakter1</p>";
        echo "<p>Jumlah Kata = $jumlah_kata1</p>";

        echo "<h3>Contoh No 2</h3>";
        $kalimat2 = "Selamat Siang";
        echo "<p>String = $kalimat2</p>";
        echo "<label>Kata 1 " . substr($kalimat2, 0, 7) . "</label><br>";
        echo "<label>Kata 2 " . substr($kalimat2, 8, 5) . "</label><br>";

        echo "<h3>Contoh No 3</h3>";
        $kalimat3 = "Welcome to Sanbercode";
        echo "<label>$kalimat3</label> <br>";
        echo "<label>" . str_replace("Sanbercode", "Makassar", $kalimat3) . "</label><br>";
    ?>
</body>
</html>