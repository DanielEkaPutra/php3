<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh Array</h1>
    <?php
    
        echo "<h3>Contoh 1</h3>";
        $trainer = ["Rezky", "Abdul", "Abdul", "Iqbal"];
        $instruktur = ["Fitria", "Fadli", "Asep"];
        print_r($trainer);
        echo "<br>";
        print_r($instruktur);

        echo "<h3>Contoh 2</h3>";
        echo "<label>Daftar Trainer</label><br>";
        echo "Total Trainer: ". count($trainer);
        echo "<br>";
        echo "<ul>";
        echo "<li>".$trainer[0]."</li>";
        echo "<li>".$trainer[1]."</li>";
        echo "<li>".$trainer[2]."</li>";
        echo "<li>".$trainer[3]."</li>";
        echo "</ul>";

        echo "<label>Daftar Instruktur</label><br>";
        echo "Total Instruktur: ". count($instruktur);
        echo "<br>";
        echo "<ul>";
        echo "<li>".$instruktur[0]."</li>";
        echo "<li>".$instruktur[1]."</li>";
        echo "<li>".$instruktur[2]."</li>";
        echo "</ul>";

        echo "<h3>Contoh 3</h3>";
        $biodata = [
            ["Nama_Depan" => "Rezky", "Nama_Belakang" => "Putra", "Asal" => "Makassar"],
            ["Nama_Depan" => "Muhammad", "Nama_Belakang" => "Hasan", "Asal" => "Bandung"],
            ["Nama_Depan" => "Iqbal", "Nama_Belakang" => "Ramadan", "Asal" => "Jakarta"]
        ];

        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
        echo $biodata[0]["Asal"];
    ?>
</body>
</html>